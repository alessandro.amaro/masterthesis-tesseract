import cv2 #notinstalledinConda
import numpy as np
import math
import pytesseract #notinstalledinConda
from statistics import mean
from pdf2image import convert_from_path #notinstalledinConda,popplermissing
import imutils #notinstalledinConda
from openpyxl import Workbook , load_workbook #notinstalledinConda
import os

#numbers zu manipulate

ShortBoxFactor = 5  #Default: 5 - increases the box size for calculated reading direction
MidBoxFactor = 2.3  #Default: 2.5 - increases the box size for probably reading direction
LongBoxFactor = 1.2  #Default: 1.2 - increases the box size for unknown direction
BoxLineExtender = 1.5 #Default: 1.5 - adds a horizontal and vetical line to extend the bounding boxes with this factor


for filename in os.listdir("results/optimice/PNGs_after_filter"):

        image = cv2.imread("results/optimice/PNGs_after_filter/" + filename)
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        threshold_image = cv2.threshold(gray_image, 200, 255, cv2.THRESH_BINARY)[1]
        print("hi")
        # defining variables and lists, set empty images
        output_with_boxes = np.zeros(threshold_image.shape, dtype="uint8")
        output_without_boxes = np.zeros(threshold_image.shape, dtype="uint8")
        output_grouped = np.zeros(threshold_image.shape, dtype="uint8")
        output_regrouped = np.zeros(threshold_image.shape, dtype="uint8")


        # apply cv2.connectedComponents
        analysis_image = cv2.connectedComponentsWithStats(threshold_image, 4, cv2.CV_32S)
        (total_labels, label_IDs, values_grouped, centroid_grouped) = analysis_image

        # calculate the mean width, height, area of the connected components

        for i in range(1, total_labels):

            # filter connected components based on their difference to the mean width, height, area
            label_mask = (label_IDs == i).astype("uint8") * 255
            label_mask_without_boxes = (label_IDs == i).astype("uint8") * 255
            # stay           thresh = cv2.threshold(label_mask, 230, 255, cv2.THRESH_BINARY)[1]
            label_contour = cv2.findContours(label_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            label_contour = label_contour[0] if len(label_contour) == 2 else label_contour[1]
            label_bounding_box = cv2.minAreaRect(label_contour[0])
            (center, (width, height), rotation_angle) = label_bounding_box

            # increasing the rectangle at the shorter side
            if width > height:

                if width > 3 * height:
                    height_fitted = height * ShortBoxFactor
                    width_fitted = width * 1.0
                elif width > 1.5 * height:
                    height_fitted = height * MidBoxFactor
                    width_fitted = width * 1.0
                else:
                    height_fitted = height * LongBoxFactor
                    width_fitted = width * LongBoxFactor
            else:
                if height > 3 * width:
                    height_fitted = height * 1
                    width_fitted = width * ShortBoxFactor
                elif height > 1.5 * width:
                    height_fitted = height * 1
                    width_fitted = width * MidBoxFactor
                else:
                    height_fitted = height * LongBoxFactor
                    width_fitted = width * LongBoxFactor

            # Creating the Final output mask and print
            label_increased_bounding_box = np.int0(cv2.boxPoints((center, (width_fitted, height_fitted), rotation_angle)))
            line_height = np.int0(cv2.boxPoints((center, (0, height_fitted * BoxLineExtender), rotation_angle)))
            line_width = np.int0(cv2.boxPoints((center, (width_fitted * BoxLineExtender, 0), rotation_angle)))
            cv2.drawContours(label_mask, [label_increased_bounding_box], 0, (255, 255, 255), 1)
            cv2.drawContours(label_mask, [line_height], 0, (255, 255, 255), 1)
            cv2.drawContours(label_mask, [line_width], 0, (255, 255, 255), 1)
            output_with_boxes = cv2.bitwise_or(output_with_boxes, label_mask)
            output_without_boxes = cv2.bitwise_or(output_without_boxes, label_mask_without_boxes)

            print("(Step 1 of 3) Reading all relevant labels " + str(
                "%.2f" % (((i + 1) / total_labels) * 100)) + " % done (total " +
                      str(i + 1) + "/" + str(total_labels) + ")")
        cv2.imwrite("results/optimice/boxes/wb_" + str(ShortBoxFactor) + "_"  + "_" + filename, output_with_boxes)
        cv2.imwrite("results/optimice/boxes/wob_" + str(ShortBoxFactor) + "_"  + "_" + filename, output_without_boxes)



