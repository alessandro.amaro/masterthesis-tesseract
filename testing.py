import sqlalchemy as sa
import urllib
import plotly.graph_objects as go
from plotly.graph_objects import Layout
import plotly.express as px
from PIL import Image
import base64
import math
import pytesseract #notinstalledinConda
from statistics import mean
from pdf2image import convert_from_bytes #notinstalledinConda,popplermissing
import imutils #notinstalledinConda
import dash
import pandas as pd
from dash import dcc, html, dash_table
from dash.dependencies import Input, Output, State
import cv2
import numpy as np
import os

#set path to tesseract here
pytesseract.pytesseract.tesseract_cmd = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"

#set SQL login data
SQL_login = str("DRIVER={SQL Server Native Client 11.0};"
                #"SERVER=HKK05L3\SQLEXPRESS;"
                #"UID=sa;"
                #"PWD=freebsd2022!;"
                "SERVER=SURFACE_PRO\MSSQLSERVER01;"
                "DATABASE=testing_MT_amaro;"
                "Trusted_Connection=yes")

#set variables to enhance detection here
#int
BorderRemovalFactor = 0.03 #Default: 0.03 - factor to reduce the text at the border. Higher -> Increase range from the borders
BorderFinenessFactor = range(1, 5) #Default: range(1, 5) - number of steps to reduce the text at the border. Higher -> Set finer detection in the border area
MaxWidth = 1.5 #Default: 1.5 - factor to set the upper range for character detection
MaxHeight = 1.5 #Default: 1.5 - factor to set the upper range for character detection
MaxArea = 1.5 #Default: 1.5 - factor to set the upper range for character detection
ShortBoxFactor = 5  #Default: 5 - increases the box size for calculated reading direction
MidBoxFactor = 2.5  #Default: 2.5 - increases the box size for probably reading direction
LongBoxFactor = 1.2  #Default: 1.2 - increases the box size for unknown direction
BoxLineExtender = 1.5 #Default: 1.5 - adds a horizontal and vetical line to extend the bounding boxes with this factor
BoxLineExtenderGroups = 1.2 #Default: 1.2 - adds a horizontal and vetical line to extend the bounding boxes for groups with this factor
RatioFactor = 3 #Default: 3 - factor for the ratio decide if a single label is a character or not

#defining variables and lists
global status
status = ""
output = []

#funtion to opmice the read text
def text_opt(text):
    text = text.replace("ØD", "Ø")
    text = text.replace("ø", "Ø")
    text = text.replace("?", "°")
    if "x" in text:
        value = text
        min = ""
        max = ""
    return (text)

#function to extract the text
def get_text(image):

    #draw lines at the border range to connect irrelevant data

    h, w, c = image.shape
    if w > h:
        for i in BorderFinenessFactor:
            print(float(BorderRemovalFactor * w / max(BorderFinenessFactor) * i),
                  float(BorderRemovalFactor * h / max(BorderFinenessFactor) * i))
            image = cv2.rectangle(image, (int(BorderRemovalFactor * 0.7 * w / max(BorderFinenessFactor) * i),
                                          int(BorderRemovalFactor * h / max(BorderFinenessFactor) * i)), (
                                      int((1 - BorderRemovalFactor * 0.7 / max(BorderFinenessFactor) * i) * w),
                                      int((1 - BorderRemovalFactor / max(BorderFinenessFactor) * i) * h)), (0, 0, 0), 1)
    if h > w:
        for i in BorderFinenessFactor:
            print(float(BorderRemovalFactor * w / max(BorderFinenessFactor) * i),
                  float(BorderRemovalFactor * h / max(BorderFinenessFactor) * i))
            image = cv2.rectangle(image, (int(BorderRemovalFactor * w / max(BorderFinenessFactor) * i),
                                          int(BorderRemovalFactor * 0.7 * h / max(BorderFinenessFactor) * i)), (
                                      int((1 - BorderRemovalFactor / max(BorderFinenessFactor) * i) * w),
                                      int((1 - BorderRemovalFactor * 0.7 / max(BorderFinenessFactor) * i) * h)),
                                  (0, 0, 0), 1)
    # Preprocess the input file
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    threshold_image = cv2.threshold(gray_image, 200, 255, cv2.THRESH_BINARY_INV)[1]
    cv2.imwrite("delete.png", threshold_image)
    # defining variables and lists, set empty images
    output_with_boxes = np.zeros(threshold_image.shape, dtype="uint8")
    output_without_boxes = np.zeros(threshold_image.shape, dtype="uint8")
    output_grouped = np.zeros(threshold_image.shape, dtype="uint8")
    output_regrouped = np.zeros(threshold_image.shape, dtype="uint8")
    width_all = []
    height_all = []
    area_all = []

    # apply cv2.connectedComponents
    analysis_image = cv2.connectedComponentsWithStats(threshold_image, 4, cv2.CV_32S)
    (total_labels, label_IDs, values_grouped, centroid_grouped) = analysis_image

    # calculate the mean width, height, area of the connected components
    for i in range(1, total_labels):
        w = values_grouped[i, cv2.CC_STAT_WIDTH]
        h = values_grouped[i, cv2.CC_STAT_HEIGHT]
        area = values_grouped[i, cv2.CC_STAT_AREA]
        width_all.append(w)
        height_all.append(h)
        area_all.append(area)
    mean_width = mean(width_all)
    mean_height = mean(height_all)
    mean_area = mean(area_all)

    # filter connected components based on their difference to the mean width, height, area
    for i in range(1, total_labels):
        global MaxWidth
        global MaxHeight
        global MaxArea
        w = values_grouped[i, cv2.CC_STAT_WIDTH]
        h = values_grouped[i, cv2.CC_STAT_HEIGHT]
        area = values_grouped[i, cv2.CC_STAT_AREA]
        keep_width = 0 < w < mean_width * float(MaxWidth)
        keep_height = 0 < h < mean_height * float(MaxHeight)
        keep_area = 0 < area < mean_area * float(MaxArea)

        if keep_width and keep_height and keep_area:
            label_mask = (label_IDs == i).astype("uint8") * 255
            label_mask_without_boxes = (label_IDs == i).astype("uint8") * 255
# stay           thresh = cv2.threshold(label_mask, 230, 255, cv2.THRESH_BINARY)[1]
            label_contour = cv2.findContours(label_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            label_contour = label_contour[0] if len(label_contour) == 2 else label_contour[1]
            label_bounding_box = cv2.minAreaRect(label_contour[0])
            (center, (width, height), rotation_angle) = label_bounding_box

            #increasing the rectangle at the shorter side
            if width > height:

                if width > 3 * height:
                    height_fitted = height * ShortBoxFactor
                    width_fitted = width * 1.0
                elif width > 1.5 * height:
                    height_fitted = height * MidBoxFactor
                    width_fitted = width * 1.0
                else:
                    height_fitted = height * LongBoxFactor
                    width_fitted = width * LongBoxFactor
            else:
                if height > 3 * width:
                    height_fitted = height * 1
                    width_fitted = width * ShortBoxFactor
                elif height > 1.5 * width:
                    height_fitted = height * 1
                    width_fitted = width * MidBoxFactor
                else:
                    height_fitted = height * LongBoxFactor
                    width_fitted = width * LongBoxFactor

            # Creating the Final output mask and print
            label_increased_bounding_box = np.int0(cv2.boxPoints((center, (width_fitted, height_fitted), rotation_angle)))
            line_height = np.int0(cv2.boxPoints((center, (0, height_fitted * BoxLineExtender), rotation_angle)))
            line_width = np.int0(cv2.boxPoints((center, (width_fitted * BoxLineExtender, 0), rotation_angle)))
            cv2.drawContours(label_mask, [label_increased_bounding_box], 0, (255, 255, 255), 1)
            cv2.drawContours(label_mask, [line_height], 0, (255, 255, 255), 1)
            cv2.drawContours(label_mask, [line_width], 0, (255, 255, 255), 1)
            output_with_boxes = cv2.bitwise_or(output_with_boxes, label_mask)
            output_without_boxes = cv2.bitwise_or(output_without_boxes, label_mask_without_boxes)
            global status
            status = ("(Step 1 of 3) Reading all relevant labels " + str("%.2f" % (((i+1)/total_labels)*100)) + " % done (total " +
                  str(i+1) + "/" + str(total_labels) + ")" )
    cv2.imwrite("output_with_boxes_prelines.png", output_with_boxes)
    cv2.imwrite("output_without_boxes.png", output_without_boxes)

    # look for related data and set it together if more than one label got connected
    analysis_increased_boxes = cv2.connectedComponentsWithStats(output_with_boxes, 4, cv2.CV_32S)
    (total_labels_grouped, label_IDs_grouped, values_grouped, centroid_grouped) = analysis_increased_boxes

    for i in range(1, total_labels_grouped):
        # choose only one grouped label
        component_Mask_line = (label_IDs_grouped == i).astype("uint8") * 255
        # remove lines and boxes for the grouped label and do analysis
        content_line = cv2.bitwise_and(output_without_boxes, component_Mask_line)
        analysis_grouped = cv2.connectedComponentsWithStats(content_line, 4, cv2.CV_32S)
        (total_labels_in_group, label_IDs_in_group, values_in_group, centroid_in_group) = analysis_grouped
        label_contour = cv2.findContours(content_line, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        label_contour = label_contour[0] if len(label_contour) == 2 else label_contour[1]
        # check if more than one label got connected, if yes add box and line in reading directory
        if total_labels_in_group > 2:
            label_contour = np.vstack(label_contour)
            contour_grouped = np.column_stack(np.where(content_line > 0))
            (center_grouped, (width_grouped, height_grouped), rotation_angle_grouped) = cv2.minAreaRect(contour_grouped)
            # Calculate minAreaRect of merged contours; determine points
            bounding_box_grouped = np.int32(cv2.boxPoints(cv2.minAreaRect(label_contour)))
            center_grouped = (bounding_box_grouped[0]+bounding_box_grouped[2])/2

            if width_grouped > height_grouped:
                line_height_grouped = np.int0(cv2.boxPoints((center_grouped, (BoxLineExtenderGroups*width_grouped, 0), (-rotation_angle_grouped+90))))
            else:
                line_height_grouped = np.int0(cv2.boxPoints((center_grouped, (0, BoxLineExtenderGroups*height_grouped), (-rotation_angle_grouped+90))))

            cv2.drawContours(content_line, [line_height_grouped], 0, (255, 255, 255), 1)
            cv2.drawContours(content_line, [bounding_box_grouped], 0, (255, 255, 255), 1)
        #    cv2.imwrite("testing.png", content_line)
            output_grouped = cv2.bitwise_or(output_grouped, content_line)

        status = ("(Step 1 of 3) Done \n" + "(Step 2 of 3) Looking for lines and connect labels " + str("%.2f" % (((i+1) / total_labels_grouped) * 100)) + " % done (total " +
              str(i+1) + "/" + str(total_labels_grouped) + ")")

    output_grouped= cv2.bitwise_or(output_with_boxes, output_grouped)
  #  cv2.imwrite("output_with_boxes.png", output_grouped)

    # apply connected components at groups
    analysis_of_groups = cv2.connectedComponentsWithStats(output_grouped, 4, cv2.CV_32S)
    (total_labels_regrouped, label_IDs_regrouped, values_regrouped, centroid_regrouped) = analysis_of_groups


    for i in range(0, total_labels_regrouped):
        component_mask_group = (label_IDs_regrouped == i).astype("uint8") * 255
        content_group = cv2.bitwise_and(output_without_boxes, component_mask_group)
        analysis_2 = cv2.connectedComponentsWithStats(content_group, 4, cv2.CV_32S)
        (totalLabels_2, label_ids_2, values_2, centroid_2) = analysis_2
# background is the first label
        if totalLabels_2 > 1:
            label_regrouped = cv2.findContours(content_group, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            label_regrouped = label_regrouped[0] if len(label_regrouped) == 2 else label_regrouped[1]
            # for single
            rectangle_around_single_contour = cv2.minAreaRect(label_regrouped[0])
            size = rectangle_around_single_contour[1]
            # for all
            label_regrouped = np.vstack(label_regrouped)
            label_bounding_box = cv2.minAreaRect(label_regrouped[0])
            pts = np.int32(cv2.boxPoints(cv2.minAreaRect(label_regrouped)))
            p1x = pts[0][0]
            p1y = pts[0][1]
            p2x = pts[1][0]
            p2y = pts[1][1]
            p3x = pts[2][0]
            p3y = pts[2][1]
            p4x = pts[3][0]
            p4y = pts[3][1]

        # check if it just one character or more, because of different ways to detect orientation
        if totalLabels_2 == 2:
            (center_single, (width_single, height_single), rotation_angle_single) = label_bounding_box
            if min(size) == 0:
                width_height_ratio=9
            else:
                width_height_ratio = max(size) / min(size)
            if width_height_ratio < RatioFactor:
                x_single, y_single, w_single, h_single = cv2.boundingRect(label_regrouped[0])
                if w_single < h_single:
                    rotation_angle_single = 0
                else:
                    rotation_angle_single = 90
                rotated= imutils.rotate_bound(content_group, rotation_angle_single)
                rotated = cv2.bitwise_not(rotated)
                text = pytesseract.image_to_string(rotated, config=("-l eng --psm 10 --oem 3 -c tessedit_char_whitelist=0123456789"))#HRMx,-+"))
                if text != "":
                    new_label_number = len(output) + 1
                    global name
                    result=[new_label_number, name, text, text, p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y]
                    output.append(result)
                    cv2.drawContours(content_group, [pts], 0, (255, 0, 0), 1)
            else:
                content_group = np.zeros(output_regrouped.shape, dtype="uint8")
        if totalLabels_2 > 2:
            sideline_1 = math.dist(pts[0], pts[1])
            sideline_2 = math.dist(pts[1], pts[2])
            pi = 3.14159265359
            if sideline_1 > sideline_2:
                dx = p2x - p1x
                dy = p1y - p2y
                rotation_angle_multi = math.atan2(dy, dx) * 180 / pi
            else:
                dx = p3x - p2x
                dy = p2y - p3y
                rotation_angle_multi = math.atan2(dy, dx) * 180 / pi
                if rotation_angle_multi < -85:
                    rotation_angle_multi = rotation_angle_multi + 180
            rotated = imutils.rotate_bound(content_group, rotation_angle_multi)
            rotated = cv2.bitwise_not(rotated)
            text = pytesseract.image_to_string(rotated, config=("-l dan --psm 6 --oem 3")) # -c tessedit_char_whitelist=0123456789HRMx,-+"))
            if text != "":
                numbers = sum(c.isdigit() for c in text)
                letters = sum(c.isalpha() for c in text)
                if 2 * numbers >= letters:
                    text_o = text_opt(text)
                    new_label_number = len(output) + 1
                    result=[new_label_number, name, text, text_o, p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y]
                    output.append(result)
                    cv2.drawContours(content_group, [pts], 0, (255, 255, 255), 1)
#            rotated = cv2.bitwise_not(rotated)
        output_regrouped = cv2.bitwise_or(output_grouped, content_group)
        status = ("(Step 1 of 3) Done \n" + "(Step 2 of 3) Done \n" + "(Step 3 of 3) Reading the lines " + str("%.2f" % (((i+1) / total_labels_regrouped) * 100)) + " % done (total " +
              str(i+1) + "/" + str(total_labels_regrouped) + ")")
    result = cv2.bitwise_and(threshold_image, output_regrouped)
    result = cv2.bitwise_not(result)
    cv2.imwrite("final_result.png", result)
    output_regrouped = cv2.bitwise_not(output_regrouped)
    cv2.imwrite("final_recogniced_boxes.png", output_regrouped)
    status = "Check the provided data"
    return output



app = dash.Dash(__name__, suppress_callback_exceptions=True)

app.layout = html.Div([
    html.Div(
    dcc.Upload(
        id="upload-data",
        children=html.Div([
            "Drag and Drop or ",
            html.A("Select File")
        ]),
        style={
            "width": "100%",
            "height": "60px",
            "lineHeight": "60px",
            "borderWidth": "1px",
            "borderStyle": "dashed",
            "borderRadius": "5px",
            "textAlign": "center",
            "margin": "10px"
        }
    )),
    html.Div(id="output-data-upload"),
    html.Div([
        html.Div(id="live-update-text"),
        dcc.Interval(
            id="interval-component",
            interval=1*1000, # in milliseconds
            n_intervals=0)
    ])
])

@app.callback(Output("live-update-text", "children"),
            State("upload-data", "filename"),
            Input("interval-component", "n_intervals"))
def update_metrics(n,input):
    global status
    if (status != "") and (n != None):
        style = {"padding": "5px", "fontSize": "16px", "white-space": "pre-line"}
        return [html.Span(status, style=style),]
    else:
        return dash.no_update


@app.callback(Output("output-data-upload", "children"),
              Input("upload-data", "contents"),
              State("upload-data", "filename"))
def update_output(contents, filename):
    if contents is not None:
        content_type, content_string = contents.split(",")
        global name
        name = os.path.splitext(filename)[0]
        decoded = base64.b64decode(content_string)
        if "pdf" in filename:
            image = convert_from_bytes(decoded, 200)
            image[0].save("image_file_from_server.png")
        else:
            pic_as_np = np.frombuffer(decoded, dtype=np.uint8)
            image = cv2.imdecode(pic_as_np, flags=1)
            cv2.imwrite("image_file_from_server.png", image)
        img = cv2.imread("image_file_from_server.png")
        output = get_text(img)
        header = ["LabelID", "DrawingID", "Text_before_opt", "QualityData", "p1x", "p1y", "p2x", "p2y", "p3x", "p3y", "p4x",
                  "p4y"]
        fig = go.Figure(layout=Layout(plot_bgcolor="rgba(0,0,0,0)"))
        children = [html.Div([
        html.H5("Choosen file: " + filename),
        html.Hr(),
        html.H4("Drag and draw undetected labels"),
        html.Div([
        dash_table.DataTable(
            id="adding-rows-table",
            style_cell={"textAlign": "left"},
            columns=[{
                "name": header[i],
                "id": "column-{}".format(i)
            } for i in range(len(header))],
            data=[
                {"column-{}".format(i): str(output[j][i]) for i in range(len(header))}
                for j in range(len(output))
            ],
            editable=True,
            row_deletable=True,
            hidden_columns=["column-1", "column-2", "column-4", "column-5", "column-6", "column-7", "column-8", "column-9", "column-10","column-11"]
        ),
    ]),
        html.Div([
            dcc.Graph(id="adding-rows-graph", figure=fig, style={"height": "100vh", "width": "100vw"},
                      config={
            "scrollZoom": True,
            "displaylogo": False,
            "modeBarButtonsToRemove": ["zoom", "lasso2d", "select", "toimage"]
            }),
        ]),
        html.Div(
            [html.Button("Export PDF", id="export-button", n_clicks=0),dcc.Download(id="my-output")]),
        html.Div(
            [
            html.Button("Store to SQL", id="save-button", n_clicks=0)]),
        html.Div(id="hidden-div", style = {"display":"none"})
    ])]
        return children

@app.callback(
    Output("adding-rows-table", "data"),
    Input("adding-rows-graph", "relayoutData"),
    State("adding-rows-table", "data"),
    State("adding-rows-table", "columns"),
    prevent_initial_call=True,)
def update_columns(relayoutData, rows, columns):
    if "shapes" in relayoutData:
        new_label_number = len(output) + 1
        x0 = relayoutData["shapes"][-1]["x0"]
        y0 = relayoutData["shapes"][-1]["y0"]
        x1 = relayoutData["shapes"][-1]["x1"]
        y1 = relayoutData["shapes"][-1]["y1"]
        global name
        new_data = [str(new_label_number), name, "", "", int(x0), int(y0), int(x1), int(y0), int(x1), int(y1), int(x0), int(y1)]
        rows.append({c['id']: new_data[int(c['id'].split("-")[-1])] for c in columns})
        return rows
    else:
        return dash.no_update

@app.callback(
    Output("my-output", "data"),
    Input("export-button", "n_clicks"),
    State("adding-rows-table", "data"),
#    State("adding-rows-table", "columns"),
    prevent_initial_call=True,)
def export(n_clicks, rows):
    if n_clicks > 0:
        for i in range(len(rows)):
            x = int(max(int(rows[i]["column-4"]), int(rows[i]["column-6"]), int(rows[i]["column-8"]), int(
                rows[i]["column-10"])))
            y = int(max(int(rows[i]["column-5"]), int(rows[i]["column-7"]), int(rows[i]["column-9"]),
                 int(rows[i]["column-11"])))
            img = cv2.imread("image_file_from_server.png")
            img = cv2.putText(img, str(rows[i]["column-0"]), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 2, cv2.LINE_AA)
            cv2.imwrite("image_file_from_server.png", img)
            global status
            status = ("(Step 1 of 1) Writing the PDF file " + str("%.2f" % (((i + 1) / len(rows)) * 100)) + " % done (total " +
             str(i + 1) + "/" + str(len(rows)) + ")")
        img = Image.open("image_file_from_server.png")
        img = img.convert("RGB")
        global name
        img.save("pdf_from_server.pdf")
        status = "PDF download complete"
        return dcc.send_file("pdf_from_server.pdf")

@app.callback(
    Output("hidden-div", "data"),
    Input("save-button", "n_clicks"),
    State("adding-rows-table", "data"),
    State("adding-rows-table", "columns"),
    prevent_initial_call=True, )
def export(n_clicks, rows, columns):
        if n_clicks > 0:
            global status
            status = ("(Step 1 of 1) Storing to SQL")
            global name
            header = []
            for c in columns:
                header.append(c["name"])
            df = pd.DataFrame(rows)
            df.columns = header
            df_export = df[["DrawingID", "LabelID", "QualityData"]]
            params = urllib.parse.quote_plus(SQL_login)
            engine = sa.create_engine("mssql+pyodbc:///?odbc_connect={}".format(params))
            df_export.to_sql("QualityData", engine, if_exists="append", index=False)
            status = "Stored to SQL"
            return dash.no_update

@app.callback(
    Output("adding-rows-graph", "figure"),
    Input("adding-rows-table", "data"))
def display_output(rows):
    img = cv2.imread("image_file_from_server.png")
    fig = px.imshow(img)
    for i in range(len(rows)):
        fig.add_trace(go.Scatter(
            x=[int(rows[i]["column-4"]), int(rows[i]["column-6"]), int(rows[i]["column-8"]), int(rows[i]["column-10"]), int(rows[i]["column-4"])],
            y=[int(rows[i]["column-5"]), int(rows[i]["column-7"]), int(rows[i]["column-9"]), int(rows[i]["column-11"]), int(rows[i]["column-5"])],
            fill="toself",line=dict(width=0),showlegend=False, mode="lines+text",hovertext="Label No "+ rows[i]["column-0"], name= "Label No " + rows[i]["column-0"]))
        fig.add_annotation(
            x=(int(rows[i]["column-4"]) + int(rows[i]["column-6"]) + int(rows[i]["column-8"]) + int(rows[i]["column-10"])) / 4,
            y=(int(rows[i]["column-5"]) + int(rows[i]["column-7"]) + int(rows[i]["column-9"]) + int(rows[i]["column-11"])) / 4,
            text= "[" + rows[i]["column-0"] + "] " + rows[i]["column-3"],
            showarrow=True,
            font=dict(color="black", size=10))
        fig.update_layout(dragmode="drawrect",modebar_add="drawrect")
        fig.update_layout({"plot_bgcolor": "rgba(100, 0, 0, 0)"})
        fig.update_traces(hoverinfo="none", hovertemplate=None)
        fig.update_xaxes(visible=False)
        fig.update_yaxes(visible=False)
    return fig

if __name__ == '__main__':
    app.run_server(debug=True)