import base64
import datetime
import io
import cv2 #notinstalledinConda
import numpy as np
import math
import pytesseract #notinstalledinConda
from statistics import mean
from pdf2image import convert_from_path, convert_from_bytes #notinstalledinConda,popplermissing
import imutils #notinstalledinConda
from openpyxl import Workbook , load_workbook #notinstalledinConda
import dash
from dash.dependencies import Input, Output, State
from dash import dcc, html, dash_table

import pandas as pd

# -*- coding: utf-8 -*-
from dash import Dash, dcc, html
from dash.dependencies import Input, Output










def text_opt(text):
    text = text.replace('ØD', 'Ø')
    text = text.replace('ø', 'Ø')
    text = text.replace('?', '°')

    if "x" in text:
        value = text
        min = ""
        max = ""

    return (text)

output = []
def save_to_xls(LabelID, Angle, text, text_o, p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y):
#    if Number_of_digits != len(text):
#        print("text postprocessing")
#        text = "wrong size_" + text

#    else:
#        print("text is ok ")
#        text = "right size_" + text
    output.append([LabelID, Angle, text, text_o, p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y])
    print(output)
    wb = load_workbook('final_ocr_results.xlsx')
    ws = wb.active
    ws["A" + str(LabelID)] = LabelID
    ws["B" + str(LabelID)] = Angle
    ws["C" + str(LabelID)] = text
    ws["D" + str(LabelID)] = text_o
    ws["E" + str(LabelID)] = p1x
    ws["F" + str(LabelID)] = p1y
    ws["G" + str(LabelID)] = p2x
    ws["H" + str(LabelID)] = p2y
    ws["I" + str(LabelID)] = p3x
    ws["J" + str(LabelID)] = p3y
    ws["K" + str(LabelID)] = p4x
    ws["L" + str(LabelID)] = p4y
    wb.save('final_ocr_results.xlsx')







# Input Settings: Set path to input file and to the tesseract.exe
path_input = "C:\\Users\\amaro\\Desktop\\202100053.18.pdf"
print(path_input)
pytesseract.pytesseract.tesseract_cmd = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"

# open the output file and delete all the stored data. When using r+ has to be 0
#f = open('sample2.txt', 'r+')
#f.truncate(0)

wb = Workbook()
wb.save('final_ocr_results.xlsx')


#    print(str(os.path.abspath(path)))
# read the input file
if path_input.endswith((".pdf", ".PDF")):

    image = convert_from_path(path_input, 300)
    image[0].save("drawings/image_file.png")
else:
    image = cv2.imread(path_input)
    cv2.imwrite("drawings/image_file.png", image)

print("Data from previous calculation and new file loaded")


image = cv2.imread("drawings/image_file.png")
h, w, c = image.shape
BorderRemovalFactor = 0.03
BorderFinenessFactor =range(1, 11)
for i in BorderFinenessFactor:
    image = cv2.rectangle(image, (int(BorderRemovalFactor * w / max(BorderFinenessFactor) * i) , int(BorderRemovalFactor * h / max(BorderFinenessFactor) * i)), (int((1 - BorderRemovalFactor / max(BorderFinenessFactor) * i) * w) , int((1 - BorderRemovalFactor / max(BorderFinenessFactor) * i) * h)), (0, 0, 0), 1)

# Preprocessing the input file
gray_input = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
threshold_input = cv2.threshold(gray_input, 230, 255, cv2.THRESH_BINARY_INV)[1]
cv2.imwrite("results/inverted_v2_High.png", threshold_input)

# defining variables and lists
output_with_boxes = np.zeros(threshold_input.shape, dtype="uint8")

output_without_boxes = np.zeros(threshold_input.shape, dtype="uint8")
w_all = []
h_all = []
area_all = []



# apply cv2.connectedComponents
analysis_of_input = cv2.connectedComponentsWithStats(threshold_input, 4, cv2.CV_32S)
(totalLabels, label_ids, values, centroid) = analysis_of_input

# calculate the mean width, height, area of the connected components
for i in range(1, totalLabels):
    w = values[i, cv2.CC_STAT_WIDTH]
    h = values[i, cv2.CC_STAT_HEIGHT]
    area = values[i, cv2.CC_STAT_AREA]
    w_all.append(w)
    h_all.append(h)
    area_all.append(area)

mean_width = mean(w_all)
mean_height = mean(h_all)
mean_area = mean(area_all)

# filter connected components based on their difference to the mean width, height, area
for i in range(1, totalLabels):
    x = values[i, cv2.CC_STAT_LEFT]
    y = values[i, cv2.CC_STAT_TOP]
    w = values[i, cv2.CC_STAT_WIDTH]
    h = values[i, cv2.CC_STAT_HEIGHT]
    area = values[i, cv2.CC_STAT_AREA]

    keep_Width = 0 < w < mean_width * 1.5
    keep_Height = 0 < h < mean_height * 1.5
    keep_Area = 0 < area < mean_area*1.5

    if keep_Width and keep_Height and keep_Area:
        component_Mask = (label_ids == i).astype("uint8") * 255
        component_Mask_without_boxes = (label_ids == i).astype("uint8") * 255
        thresh = cv2.threshold(component_Mask, 230, 255, cv2.THRESH_BINARY)[1]
        contour = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contour = contour[0] if len(contour) == 2 else contour[1]
        rectangle_around_contour = cv2.minAreaRect(contour[0])
        (center, (width, height), angle_of_rotation) = rectangle_around_contour

        #increasing the rectangle at the shorter side
        if width > height:
            if width > 3 * height:
                height_fitted = height * 5
                width_fitted = width * 1.0
            elif width > 1.5 * height:
                height_fitted = height * 2.5
                width_fitted = width * 1.0
            else:
                height_fitted = height * 1.2
                width_fitted = width * 1.2
        else:
            if height > 3 * width:
                height_fitted = height * 1
                width_fitted = width * 5
            if height > 1.5 * width:
                height_fitted = height * 1
                width_fitted = width * 2.5
            else:
                height_fitted = height * 1.2
                width_fitted = width * 1.2

        # Creating the Final output mask and print
        box = np.int0(cv2.boxPoints((center, (width_fitted, height_fitted), angle_of_rotation)))
        line_1 = np.int0(cv2.boxPoints((center, (0, height_fitted*1.5), angle_of_rotation)))
        line_2 = np.int0(cv2.boxPoints((center, (width_fitted*1.5, 0), angle_of_rotation)))

        cv2.drawContours(component_Mask, [box], 0, (255, 255, 255), 1)
        cv2.drawContours(component_Mask, [line_1], 0, (255, 255, 255), 1)
        cv2.drawContours(component_Mask, [line_2], 0, (255, 255, 255), 1)

        output_with_boxes = cv2.bitwise_or(output_with_boxes, component_Mask)
        output_without_boxes = cv2.bitwise_or(output_without_boxes, component_Mask_without_boxes)
        print("(1/3) Reading all relevant labels " + str("%.2f" % (((i+1)/totalLabels)*100)) + " % done (total " +
              str(i+1) + "/" + str(totalLabels) + ")" )

cv2.imwrite("output_with_boxes_prelines.png", output_with_boxes)
cv2.imwrite("output_without_boxes.png", output_without_boxes)


analysis_of_lines = cv2.connectedComponentsWithStats(output_with_boxes, 4, cv2.CV_32S)
(totalLabels, label_ids, values, centroid) = analysis_of_lines
output_of_lines = np.zeros(output_with_boxes.shape, dtype="uint8")


for i in range(1, totalLabels):
    component_Mask_line = (label_ids == i).astype("uint8") * 255
    content_line = cv2.bitwise_and(output_without_boxes, component_Mask_line)
#    cv2.imwrite("ta/output_lines"+str(i)+".png", content_line)
    analysis_of_lines_2 = cv2.connectedComponentsWithStats(content_line, 4, cv2.CV_32S)
    (totalLabels2, label_ids2, values, centroid) = analysis_of_lines_2
    contour = cv2.findContours(content_line, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    contour = contour[0] if len(contour) == 2 else contour[1]
    if totalLabels2 > 2:
  #      print(totalLabels2)
 #       print("More digit number at label ID: " + str(i))
        contour = np.vstack(contour)
        coords = np.column_stack(np.where(content_line > 0))
        (center, (width, height), angle) = cv2.minAreaRect(coords)

 #       (h, w) = image.shape[:2]
 #       center = (w // 2, h // 2)
        # Calculate minAreaRect of merged contours; determine points

        pts = np.int32(cv2.boxPoints(cv2.minAreaRect(contour)))
  #      print(pts)

        line1 = math.dist(pts[0], pts[1])
        line2 = math.dist(pts[1], pts[2])
        center = (pts[0]+pts[2])/2
  #      w= line1
  #      h= line2



        if width > height:
            line_1 = np.int0(cv2.boxPoints((center, (1.2*width, 0), (-angle+90))))
 #           print("line1 is bigger")
        else:
            line_1 = np.int0(cv2.boxPoints((center, (0, 1.2*height), (-angle+90))))
 #           print("line2is bigger")



        cv2.drawContours(content_line, [line_1], 0, (255, 255, 255), 1)
   #     cv2.imwrite("ta/output_lines" + str(i) + ".png", content_line)
        cv2.drawContours(content_line, [pts], 0, (255, 255, 255), 1)

        output_of_lines = cv2.bitwise_or(output_of_lines, content_line)
 #   cv2.imwrite("ta/output_lines.png", output_of_lines)
    print("(2/3) Looking for lines and connect labels " + str("%.2f" % (((i+1) / totalLabels) * 100)) + " % done (total " +
          str(i+1) + "/" + str(totalLabels) + ")")

output_with_boxes= cv2.bitwise_or(output_with_boxes, output_of_lines)
cv2.imwrite("output_with_boxes.png", output_with_boxes)








# apply connected components at groups
analysis_of_groups = cv2.connectedComponentsWithStats(output_with_boxes, 4, cv2.CV_32S)
(totalLabels, label_ids, values, centroid) = analysis_of_groups
#print("NUMBER OF TOTAL LABELS_" + str(totalLabels))
output_of_groups = np.zeros(output_with_boxes.shape, dtype="uint8")






for i in range(0, totalLabels):

    x = values[i, cv2.CC_STAT_LEFT]
    y = values[i, cv2.CC_STAT_TOP]
    w = values[i, cv2.CC_STAT_WIDTH]
    h = values[i, cv2.CC_STAT_HEIGHT]

    component_Mask_group = (label_ids == i).astype("uint8") * 255
    content_group = cv2.bitwise_and(output_without_boxes, component_Mask_group)

    contour= cv2.findContours(content_group, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    contour = contour[0] if len(contour) == 2 else contour[1]

    analysis_2 = cv2.connectedComponentsWithStats(content_group, 4, cv2.CV_32S)
    (totalLabels_2, label_ids_2, values_2, centroid_2) = analysis_2
#    print("Number of grouped labels: " + str(totalLabels_2))
#    cv2.imwrite("ta/before_rot" + str(i) + ".png", content_group)
    # check if it just one character or more, because of different ways to detect orientation
    if totalLabels_2 == 2:


        contours = cv2.findContours(content_group, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[0] if len(contours) == 2 else contours[1]
        rectangle_around_contour = cv2.minAreaRect(contours[0])
        (center, (width, height), angle_of_rotation) = rectangle_around_contour
        box = np.int0(cv2.boxPoints(rectangle_around_contour))
        size = rectangle_around_contour[1]

        contour = np.vstack(contours)
        print("here")
        pts = np.int32(cv2.boxPoints(cv2.minAreaRect(contour)))

        p1x = pts[0][0]
        p1y = pts[0][1]
        p2x = pts[1][0]
        p2y = pts[1][1]
        p3x = pts[2][0]
        p3y = pts[2][1]
        p4x = pts[3][0]
        p4y = pts[3][1]

        print(p1x)
        print(p1y)
        print(p2x)
        print(p2y)
        print(p3x)
        print(p3y)
        print(p4x)
        print(p4y)


        ratio = max(size) / min(size)

        isSymbol = ratio < 3

        if isSymbol:
            x, y, w, h = cv2.boundingRect(contour[0])

            if w < h:
                angle_of_rotation = 0
            else:
                angle_of_rotation = 90
            rotated_with_box = imutils.rotate_bound(component_Mask_group, angle_of_rotation)
            rotated= imutils.rotate_bound(content_group, angle_of_rotation)

    #        content_group = cv2.bitwise_not(content_group)
            rotated = cv2.bitwise_not(rotated)
            cv2.imwrite("ta/Rotated_label_ID_" + str(i) + "single.png", rotated)
            text = pytesseract.image_to_string(rotated, config=(
                "-l eng --psm 10 --oem 3 -c tessedit_char_whitelist=0123456789"))#HRMx,-+"))
            if text != "":
                save_to_xls(i, angle_of_rotation, text, text, p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y)
                cv2.drawContours(content_group, [box], 0, (255, 0, 0), 1)

        else:
 #           print("Contour No " + str(i) + "is _________________not relevant")
            content_group = np.zeros(output_of_groups.shape, dtype="uint8")

    if totalLabels_2 > 2:
 #       print("More digit number at label ID: " + str(i))
        contour = np.vstack(contour)
        coords = np.column_stack(np.where(content_group > 0))
        angle = cv2.minAreaRect(coords)[-1]
        (h, w) = image.shape[:2]
        center = (w // 2, h // 2)
        # Calculate minAreaRect of merged contours; determine points
        pts = np.int32(cv2.boxPoints(cv2.minAreaRect(contour)))

        print("there")

        p1x = pts[0][0]
        p1y = pts[0][1]
        p2x = pts[1][0]
        p2y = pts[1][1]
        p3x = pts[2][0]
        p3y = pts[2][1]
        p4x = pts[3][0]
        p4y = pts[3][1]

        print(p1x)
        print(p1y)
        print(p2x)
        print(p2y)
        print(p3x)
        print(p3y)
        print(p4x)
        print(p4y)



        line1 = math.dist(pts[0], pts[1])
        line2 = math.dist(pts[1], pts[2])
        pi = 3.14159265359

        if line1 > line2:
            dx = pts[1][0] - pts[0][0]
            dy = pts[0][1] - pts[1][1]
            angle = math.atan2(dy, dx) * 180 / pi
        else:
            dx = pts[2][0] - pts[1][0]
            dy = pts[1][1] - pts[2][1]
            angle = math.atan2(dy, dx) * 180 / pi
            if angle < -85:
                angle = angle + 180

        rotated = imutils.rotate_bound(content_group, angle)
        rotated = cv2.bitwise_not(rotated)
      #  cv2.putText(rotated, "Angle: {:.2f} degrees".format(angle), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255),2)
#        print("[INFO] angle: {:.3f}".format(angle))
#       cv2.imwrite("ta/Input_bofore_rot_" + str(i) + ".png", content_group)
        cv2.imwrite("ta/Rotated_label_ID_" + str(i) + ".png", rotated)


        text = pytesseract.image_to_string(rotated, config=(
            "-l dan --psm 6 --oem 3")) # -c tessedit_char_whitelist=0123456789HRMx,-+"))
        if text != "":
            numbers = sum(c.isdigit() for c in text)
            letters = sum(c.isalpha() for c in text)
            if 2*numbers >= letters:
                text_o = text_opt(text)
                save_to_xls(i, angle, text, text_o, p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y)
                cv2.drawContours(content_group, [pts], 0, (255, 255, 255), 1)
   #         else:
                #save_to_xls(i, totalLabels_2, "irrelavant" + text, text, p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y)
        rotated = cv2.bitwise_not(rotated)

    output_of_groups = cv2.bitwise_or(output_of_groups, content_group)
    print("(3/3) Reading the lines " + str("%.2f" % (((i+1) / totalLabels) * 100)) + " % done (total " +
          str(i+1) + "/" + str(totalLabels) + ")")

output_of_groups = cv2.bitwise_not(output_of_groups)

cv2.imwrite("final_recogniced_boxes.png", output_of_groups)

result = cv2.bitwise_or(threshold_input, output_of_groups)

cv2.imwrite("final_result.png", result)
print("done")




