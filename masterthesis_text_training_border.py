import cv2 #notinstalledinConda
import numpy as np
import math
import pytesseract #notinstalledinConda
from statistics import mean
from pdf2image import convert_from_path #notinstalledinConda,popplermissing
import imutils #notinstalledinConda
from openpyxl import Workbook , load_workbook #notinstalledinConda
import os

#numbers zu manipulate

BorderRemovalFactor = 0.03 #Default: 0.03 - factor to reduce the text at the border. Higher -> Increase range from the borders
BorderFinenessFactor = range(1, 5) #Default: range(1, 11) - number of steps to reduce the text at the border. Higher -> Set finer detection in the border area


for filename in os.listdir("results/optimice/PNGs"):

        image = cv2.imread("results/optimice/PNGs/" + filename)
        print("Data from previous calculation and new file loaded")
        h, w, c = image.shape
        if w > h:
            for i in BorderFinenessFactor:
                print(float(BorderRemovalFactor * w / max(BorderFinenessFactor) * i),
                      float(BorderRemovalFactor * h / max(BorderFinenessFactor) * i))
                image = cv2.rectangle(image, (int(BorderRemovalFactor * 0.7 * w / max(BorderFinenessFactor) * i),
                                              int(BorderRemovalFactor * h / max(BorderFinenessFactor) * i)), (
                                      int((1 - BorderRemovalFactor * 0.7 / max(BorderFinenessFactor) * i) * w),
                                      int((1 - BorderRemovalFactor / max(BorderFinenessFactor) * i) * h)), (0, 0, 0), 1)
        if h > w:
            for i in BorderFinenessFactor:
                print(float(BorderRemovalFactor * w / max(BorderFinenessFactor) * i),
                      float(BorderRemovalFactor * h / max(BorderFinenessFactor) * i))
                image = cv2.rectangle(image, (int(BorderRemovalFactor * w / max(BorderFinenessFactor) * i),
                                              int(BorderRemovalFactor * 0.7 * h / max(BorderFinenessFactor) * i)), (
                                      int((1 - BorderRemovalFactor / max(BorderFinenessFactor) * i) * w),
                                      int((1 - BorderRemovalFactor * 0.7 / max(BorderFinenessFactor) * i) * h)), (0, 0, 0), 1)

        cv2.imwrite("results/optimice/borders/BorderFinenessFactorr_" + str(BorderFinenessFactor) + "_" + filename, image)

