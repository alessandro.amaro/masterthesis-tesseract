import cv2 #notinstalledinConda
import numpy as np
import math
import pytesseract #notinstalledinConda
from statistics import mean
from pdf2image import convert_from_path #notinstalledinConda,popplermissing
import imutils #notinstalledinConda
from openpyxl import Workbook , load_workbook #notinstalledinConda
import os

#numbers zu manipulate

MaxWidth = 2.4 #Default: 1.5 - factor to set the upper range for character detection
MaxHeight = 2.4 #Default: 1.5 - factor to set the upper range for character detection
MaxArea = 1.1 #Default: 1.5 - factor to set the upper range for character detection


for filename in os.listdir("results/optimice/PNGs_after_borders"):

        image = cv2.imread("results/optimice/PNGs_after_borders/" + filename)
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        threshold_image = cv2.threshold(gray_image, 200, 255, cv2.THRESH_BINARY_INV)[1]
        print("hi")
        # defining variables and lists, set empty images
        output_with_boxes = np.zeros(threshold_image.shape, dtype="uint8")
        output_without_boxes = np.zeros(threshold_image.shape, dtype="uint8")
        output_grouped = np.zeros(threshold_image.shape, dtype="uint8")
        output_regrouped = np.zeros(threshold_image.shape, dtype="uint8")
        width_all = []
        height_all = []
        area_all = []

        # apply cv2.connectedComponents
        analysis_image = cv2.connectedComponentsWithStats(threshold_image, 4, cv2.CV_32S)
        (total_labels, label_IDs, values_grouped, centroid_grouped) = analysis_image

        # calculate the mean width, height, area of the connected components
        for i in range(1, total_labels):
            w = values_grouped[i, cv2.CC_STAT_WIDTH]
            h = values_grouped[i, cv2.CC_STAT_HEIGHT]
            area = values_grouped[i, cv2.CC_STAT_AREA]
            width_all.append(w)
            height_all.append(h)
            area_all.append(area)
        mean_width = mean(width_all)
        mean_height = mean(height_all)
        mean_area = mean(area_all)

        # filter connected components based on their difference to the mean width, height, area
        for i in range(1, total_labels):

            w = values_grouped[i, cv2.CC_STAT_WIDTH]
            h = values_grouped[i, cv2.CC_STAT_HEIGHT]
            area = values_grouped[i, cv2.CC_STAT_AREA]
            keep_width = 0 < w < mean_width * float(MaxWidth)
            keep_height = 0 < h < mean_height * float(MaxHeight)
            keep_area = 0 < area < mean_area * float(MaxArea)

            if keep_width and keep_height and keep_area:
                label_mask = (label_IDs == i).astype("uint8") * 255
                label_mask_without_boxes = (label_IDs == i).astype("uint8") * 255
                output_without_boxes = cv2.bitwise_or(output_without_boxes, label_mask_without_boxes)
                print("(Step 1 of 3) Reading all relevant labels " + str(
                    "%.2f" % (((i + 1) / total_labels) * 100)) + " % done (total " +
                          str(i + 1) + "/" + str(total_labels) + ")")


        cv2.imwrite("results/optimice/filter/wh_" + str(MaxWidth) + "_a_" + str(MaxArea) + "_" + filename, output_without_boxes)

