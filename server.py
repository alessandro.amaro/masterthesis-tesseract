import base64
import datetime
import io
from pdf2image import convert_from_path, convert_from_bytes #notinstalledinConda,popplermissing
import dash
from dash.dependencies import Input, Output, State
from dash import dcc, html, dash_table
import base64
import datetime
import io
import cv2 #notinstalledinConda
import numpy as np
import math
import pytesseract #notinstalledinConda
from statistics import mean
from pdf2image import convert_from_path, convert_from_bytes #notinstalledinConda,popplermissing
import imutils #notinstalledinConda
import plotly.express as px
from openpyxl import Workbook , load_workbook #notinstalledinConda
import dash
import json
from dash.dependencies import Input, Output, State
from dash import dcc, html, dash_table
import pandas as pd






external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    dcc.Upload(
        id='upload-data',
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
        multiple=True
    ),
    html.Div(id='output-data-upload'),

])

def parse_contents(contents, filename, date):
    content_type, content_string = contents.split(',')
    print(content_type)
    decoded = base64.b64decode(content_string)

    try:
        if 'pdf' in filename:
            image = convert_from_bytes(decoded, 300)
            image[0].save("image_file_from_server.png")
            #contents = base64.b64encode(open("image_file_from_server.png", 'rb').read()).decode('ascii')
            img = cv2.imread("image_file_from_server.png")
            output = [[1, 4, '165\n', '165\n', 2414, 433, 2494, 433, 2494, 474, 2414, 474], [2, 11, '0,0\n153 -0,2\n', '0,0\n153 -0,2\n', 2359, 521, 2550, 521, 2550, 619, 2359, 619], [5, 10, '0,0\n28 -0,2\n', '0,0\n28 -0,2\n', 1253, 1064, 1253, 898, 1351, 898, 1351, 1064], [6, 3, '40\n', '40\n', 1094, 953, 1135, 953, 1135, 1010, 1094, 1010], [7, 3, '30\n', '30\n', 1164, 953, 1205, 953, 1205, 1009, 1164, 1009], [15, 3, 'M4\n', 'M4\n', 3507, 1282, 3535, 1253, 3589, 1307, 3561, 1336], [17, 3, '15\n', '15\n', 1543, 1465, 1543, 1425, 1593, 1425, 1593, 1465], [18, 4, '160\n', '160\n', 2384, 1496, 2465, 1496, 2465, 1537, 2384, 1537], [19, 19, 'irrelavant1X 45? an allen Kanten\n', '1X 45? an allen Kanten\n', 3552, 2045, 3552, 2004, 4156, 2004, 4156, 2045], [20, 6, 'Ø 4,50\n', 'Ø 4,50\n', 1437, 1998, 1591, 1994, 1592, 2046, 1438, 2050], [21, 4, 'irrelavantA-ÅA\n', 'A-ÅA\n', 2469, 2056, 2469, 2017, 2566, 2017, 2566, 2056], [23, 4, '920?\n', '920°\n', 1613, 2341, 1691, 2341, 1691, 2383, 1613, 2383], [24, 2, '8\n', '8\n', 1164, 2399, 1205, 2399, 1205, 2425, 1164, 2425], [25, 2, '3\n', '3\n', 1235, 2399, 1276, 2399, 1276, 2425, 1235, 2425], [26, 6, 'D9,18\n', 'D9,18\n', 1352, 2399, 1506, 2395, 1507, 2448, 1353, 2452], [27, 24, 'irrelavantOBERFLARCHENBEARBEITUNG:\n', 'OBERFLARCHENBEARBEITUNG:\n', 3831, 2749, 4160, 2749, 4160, 2767, 3831, 2767], [28, 10, 'irrelavantENTGRATEN\n', 'ENTGRATEN\n', 3594, 2749, 3720, 2749, 3720, 2767, 3594, 2767], [29, 22, 'irrelavantOBERFLACHENGUTE:\n', 'OBERFLACHENGUTE:\n', 3038, 2767, 3038, 2746, 3261, 2746, 3261, 2767], [30, 27, 'irrelavantWENN NICHT ANDERS DEFINIERT:\n', 'WENN NICHT ANDERS DEFINIERT:\n', 2555, 2773, 2555, 2755, 2905, 2755, 2905, 2773], [31, 17, 'irrelavantUND SCHARFE\nKANTEN\n', 'UND SCHARFE\nKANTEN\n', 3594, 2824, 3594, 2777, 3747, 2777, 3747, 2824], [32, 76, 'irrelavantBEMASSUNGEN SIND IN MILLIMETER\nALLGEMEINTOLERANZEN\n\nDIN ISO 2768-1:\n\nDIN ISO 2768-2:K\n', 'BEMASSUNGEN SIND IN MILLIMETER\nALLGEMEINTOLERANZEN\n\nDIN ISO 2768-1:\n\nDIN ISO 2768-2:K\n', 2555, 2892, 2555, 2785, 2935, 2785, 2935, 2892], [37, 8, 'irrelavantBRECHEN\n', 'BRECHEN\n', 3594, 2837, 3693, 2837, 3693, 2855, 3594, 2855], [38, 2, '3\n', '3\n', 2729, 2850, 2747, 2850, 2747, 2862, 2729, 2862], [39, 12, 'irrelavantBENENNUNG:\n', 'BENENNUNG:\n', 3815, 2958, 3958, 2958, 3958, 2976, 3815, 2976], [40, 5, 'irrelavantNAME\n', 'NAME\n', 3478, 2959, 3543, 2959, 3543, 2975, 3478, 2975], [41, 6, 'irrelavantDATUM\n', 'DATUM\n', 3695, 2959, 3772, 2959, 3772, 2976, 3695, 2976], [42, 22, 'irrelavantKabelschutz AWEA\nDeckel\n', 'Kabelschutz AWEA\nDeckel\n', 3976, 3163, 3976, 3017, 4708, 3017, 4708, 3163], [43, 11, 'irrelavantGEZEICHNET\n', 'GEZEICHNET\n', 3288, 3002, 3421, 3002, 3421, 3020, 3288, 3020], [44, 5, 'irrelavantEbert\n', 'Ebert\n', 3476, 3002, 3531, 3002, 3531, 3020, 3476, 3020], [45, 11, '08.11.2021\n', '08.11.2021\n', 3684, 3002, 3791, 3002, 3791, 3020, 3684, 3020], [46, 9, 'irrelavantGEPROFT\n', 'GEPROFT\n', 3289, 3071, 3289, 3050, 3383, 3050, 3383, 3071], [47, 12, 'irrelavantFREIGEGEBEN\n', 'FREIGEGEBEN\n', 3290, 3104, 3435, 3104, 3435, 3122, 3290, 3122], [48, 12, 'irrelavantWERKSTOFF:\n', 'WERKSTOFF:\n', 3290, 3198, 3419, 3198, 3419, 3216, 3290, 3216], [49, 14, 'irrelavantZEICHNUNGSNR.\n', 'ZEICHNUNGSNR.\n', 3815, 3211, 3993, 3211, 3993, 3229, 3815, 3229], [50, 3, 'A3\n', 'A3\n', 4737, 3218, 4792, 3218, 4792, 3252, 4737, 3252], [51, 13, '202100053.18\n', '202100053.18\n', 4083, 3238, 4430, 3238, 4430, 3279, 4083, 3279], [52, 17, 'irrelavantPTFE (allgemein)\n', 'PTFE (allgemein)\n', 3401, 3250, 3679, 3250, 3679, 3286, 3401, 3286], [53, 16, 'GEWICHT; 104,04\n', 'GEWICHT; 104,04\n', 3290, 3370, 3290, 3352, 3475, 3352, 3475, 3370], [54, 10, 'irrelavantBLATT I VON 1\n', 'BLATT I VON 1\n', 4340, 3352, 4491, 3352, 4491, 3370, 4340, 3370], [55, 10, 'irrelavantAENDERUNG\n', 'AENDERUNG\n', 2610, 3355, 2750, 3355, 2750, 3373, 2610, 3373], [56, 5, 'irrelavantNAME\n', 'NAME\n', 3060, 3353, 3125, 3353, 3125, 3369, 3060, 3369], [57, 6, 'irrelavantDATUM\n', 'DATUM\n', 3179, 3353, 3256, 3353, 3256, 3370, 3179, 3370], [58, 15, 'irrelavantMASSSTAB:1;1\n', 'MASSSTAB:1;1\n', 3815, 3352, 3958, 3352, 3958, 3370, 3815, 3370], [59, 3, 'irrelavantNR\n', 'NR\n', 2553, 3355, 2580, 3355, 2580, 3371, 2553, 3371]]
            df = pd.DataFrame(output, columns=["LabelID", "Number_of_digits", "text", "text_o", "p1x", "p1y", "p2x", "p2y", "p3x", "p3y", "p4x", "p4y"])
            fig = px.imshow(img)
            fig.update_layout(dragmode="drawrect")
            config = {
                "modeBarButtonsToAdd": [
                    "drawline",
                    "drawrect",
                    "eraseshape",
                ]
            }
    except Exception as e:
        print(e)
        return html.Div([
            'There was an error processing this file.'
        ])

    return html.Div([
        html.H5(filename),
        html.Hr(),
        html.H4(
            "Drag and draw annotations - use the modebar to pick a different drawing tool"
        ),
        dcc.Graph(id="graph-pic", figure=fig , config=config),
        dash_table.DataTable(df.to_dict('records'), [{"name": i, "id": i} for i in df.columns]),
        dcc.Markdown("Characteristics of shapes"),
        html.Pre(id="annotations-data-pre"),
    ])

@app.callback(Output('output-data-upload', 'children'),
              Input('upload-data', 'contents'),
              State('upload-data', 'filename'),
              State('upload-data', 'last_modified'))
def update_output(list_of_contents, list_of_names, list_of_dates):
    if list_of_contents is not None:
        children = [
            parse_contents(c, n, d) for c, n, d in
            zip(list_of_contents, list_of_names, list_of_dates)]
        return children



@app.callback(
    Output("annotations-data-pre", "children"),
    Input("graph-pic", "relayoutData"),
    prevent_initial_call=True,

)
def on_new_annotation(relayout_data):
        #relayout_data = {'shapes': [{'editable': True, 'xref': 'x', 'yref': 'y', 'layer': 'above', 'opacity': 1, 'line': {'color': '#444', 'width': 4, 'dash': 'solid'}, 'type': 'line', 'x0': 1025.8775307932208, 'y0': 1967.406195659022, 'x1': 2610.13559530935, 'y1': 1209.2255504977318}, {'editable': True, 'xref': 'x', 'yref': 'y', 'layer': 'above', 'opacity': 1, 'line': {'color': '#444', 'width': 4, 'dash': 'solid'}, 'type': 'line', 'x0': 2881.7226920835433, 'y0': 2182.412647271925, 'x1': 3662.53559530935, 'y1': 1016.8513569493447}, {'editable': True, 'xref': 'x', 'yref': 'y', 'layer': 'above', 'opacity': 1, 'line': {'color': '#444', 'width': 4, 'dash': 'solid'}, 'type': 'line', 'x0': 2621.451724341608, 'y0': 2205.0449053364414, 'x1': 1784.0581759545112, 'y1': 1492.1287763041832}]}

        if "shapes" in relayout_data:
            print(relayout_data)
            return json.dumps(relayout_data["shapes"], indent=2)
        else:
            return dash.no_update






if __name__ == '__main__':
    app.run_server(debug=True)