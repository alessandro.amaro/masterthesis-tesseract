import numpy as np
import os

all_files = os.listdir("drawings/all")
testing_list = np.random.choice(all_files, 5)

with open('random5of25.txt', 'w') as f:
    for line in testing_list:
        f.write(f"{line}\n")
